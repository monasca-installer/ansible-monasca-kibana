# Supported Kibana version
**ansible-monasca-kibana** support [Kibana 4.3.x](https://www.elastic.co/products/kibana)

# Variables

## Optional

* **download_timeout** - defines a time for timeout when downloading ELK tar files, defaults to 600
* **download_tmp_dir** - location where logstash is being download (default /tmp)
* **kibana_port** - port Kibana should bind to, default 5601
* **kibana_host** - host Kibana should bind to, default 0.0.0.0
* **kibana_wait_for_timeout** - how long to wait for service to start
* **kibana_node_options** - value for environment variable NODE_OPTIONS (default '')
* **kibana_uncompress_dest** - location where kibana should be unpacked
* **kibana_dest** - location where symlink to **kibana_uncompress_dest** is created
* **kibana_log_dir** - location where kibana writes log,
* **kibana_log_level** - One of the following values is permitted:
 * *silent* - turns logging off
 * *quiet* - only exceptions are logged
 * *verbose* - highest logging level
* **kibana_run_mode**:
  * **Deploy** - performs full installation, includes following phases:
   * *Install*
   * *Configure*
   * *Start*
   * *Status*
  * **Install** - only installation, does not include configuration or starting
  * **Configure** - useful for configuration (reconfiguration).
  * **Start** - starts Kibana service
  * **Stop** - stops Kibana service,
  * **Status** - verifies Kibana status
* **kibana_install_user_group** - should install user (```kibana_user```) and group (```kibana_group```), defaults to ```True```

# ElasticSearch communication
* **elasticsearch_url**: default "http://localhost:9200"
* **elasticsearch_preserve_host**:default  true
* **elasticsearch_ping_timeout**: default 1500
* **elasticsearch_request_timeout**: default 300000
* **elasticsearch_shard_timeout**: default 0
* **elasticsearch_startup_timeout**: default 5000

For more information please refer to [official documentation](https://www.elastic.co/guide/en/kibana/current/kibana-server-properties.html)

# Process and resources control (user,group)

Following variables are used to narrow down Kibana access only to those
resources which it actually needs.

* **kibana_user** - user name for Kibana resources
* **kibana_group** - group name for Kibana resources

# Installing plugins Process

Installing plugins is possible with this role with the help of **kibana_plugins**
variable. It should always be a dictionary.
Please check below to see what are possible entries that can be part
of this dictionary. For more details please visit following
[page](https://www.elastic.co/guide/en/kibana/current/kibana-plugins.html).

<!-- TODO(trebskit) updating, removing on demand ?-->

## Install by name

```yml
kibana_plugins:
  'org/sample-plugin/latest':
    configuration:
      'sample-plugin.enabled': true,
      'sample-plugin.some_property': 1000
```

## Install by url
```yml
kibana_plugins:
  'org/sample-plugin/latest':
    url: 'http:///example.org/org/sample-plugin/letest.tgz'
    configuration:
      'sample-plugin.enabled': true,
      'sample-plugin.some_property': 1000
```

Each item in **kibana_plugins** should come with configuration entry.
Please note that this configuration will be reflected in 1:1 ration to kibana.yml
file.

```yml
'org/sample-plugin/latest':
  configuration:
    'sample-plugin.enabled': true,
    'sample-plugin.some_property': 1000
```

Pay attention to keys in configuration, their prefix must reflect plugin name.
Also make sure that values assigned to configuration keys are in appropriate
format.

## License

Apache License, Version 2.0

## Author Information

Tomasz Trebski
